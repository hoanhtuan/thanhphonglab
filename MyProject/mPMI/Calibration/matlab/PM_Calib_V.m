clear all; close all;

% Voltage Value read from ADC
Nv = [172 279 436 586 706 845 992 1123 1264 1408];
% Reference Voltage
V = [1.232 2.004 3.125 4.20 5.06 6.05 7.11 8.04 9.04 10.08];
V1 = V * 1000; % in miliVolt

% Calculate Calibration factor
factorV = polyfit(Nv, V1, 1);
fprintf('factorV: %f*x1 + %f', factorV);
% Calculate Voltage value after using calbration factor
V12 = polyval(factorV, Nv);
% Display only fraction 
%rats (factorV)


% figure (1)
figure (1)
plot (Nv, V1, '-ob')
%plot (Nv, V1, '-sm')
hold on;
plot (Nv, V12, '-*r')

% Calculate calibration factor that will be used be microcontroller
n = 10; % my factor
myFactor = [round(2^n * factorV(1)) round(factorV(2))];

% Calculate back voltage values that apply calibration factor of
% microcontroller
V13 = zeros(1, length(Nv));
for cnt =1:length(Nv)
    V13(cnt) = Nv(cnt) * myFactor(1)/2^(n) + myFactor(2);
end

% Calculate error
err2 = (V12 - V1) ./ (V1) * 100; % Applied calib factor just used for matlab
err3 = (V13 - V1) ./ (V1) * 100; % Applied calib factor used for microcontroller

% Display on command window
fprintf('\nn: %d',n);
fprintf('\nMaxErr2: %f \nMaxErr3: %f',max(err2), max(err3));
%[x; myFactor]
%disp(x)
fprintf('\nMyFactor:')
disp(myFactor)


figure (2)
plot (Nv, V1, '-ob')
hold on;
%plot (Nv, V12, '-sm')
plot (Nv, V13, '-*r')
grid on;
legend('Reference Voltage','Measured Voltage',2);
title('Calibration Result','Fontsize',12);
xlabel('Sample Value');
ylabel('Voltage (mV)');

% Display the result
disp ('Reference Measured   Error');
disp ('|      V| |     V| |    %|');
dispResult = sprintf('%8.3f %8.3f %8.3f \n', [V1/1000;  V13/1000; err3]);
disp(dispResult)
%
fprintf('MaxErr: %f\n', max(err3));