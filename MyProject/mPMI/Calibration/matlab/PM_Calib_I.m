clear all; close all;

% Current Value read from ADC
Ni = [2086 2100 2116 2130 2144 2160 2175 2188 2200 2232 2260];
% Reference Current
I1 = [204 300 412 506 621 725 816 923 1026 1237 1452]; % in miliAmp

% Calculate Calibration factor
factorI = polyfit(Ni, I1, 1);
fprintf('factorI: %f*x1 + %f', factorI);
% Calculate Current value after using calbration factor
I12 = polyval(factorI, Ni);
% Display only fraction 
%rats (factorV)

figure (1)
plot (Ni, I1, '-ob')
hold on;
plot (Ni, I12, '-*r')

% Calculate calibration factor that will be used be microcontroller
n = 10; % my factor
myFactor = [round(2^n * factorI(1)) round(factorI(2))];

% Calculate back Current values that apply calibration factor of
% microcontroller
I13 = zeros(1, length(Ni));
for cnt =1:length(Ni)
    I13(cnt) = Ni(cnt) * myFactor(1)/2^(n) + myFactor(2);
end

% Calculate error
err2 = (I12 - I1) ./ (I1) * 100;
err3 = (I13 - I1) ./ (I1) * 100;

% Display on command window
%disp([n max(err)])
fprintf('\nn: %d',n);
fprintf('\nMaxErr2: %f \nMaxErr3: %f',max(err2), max(err3));
%[x; myFactor]
%disp(x)
fprintf('\nMyFactor:')
disp(myFactor)


figure (2)
plot (Ni, I1, '-ob')
hold on;
plot (Ni, I13, '-*r')
grid on;
legend('Reference Current','Measured Current',2);
title('Calibration Result','Fontsize',12);
xlabel('Sample Value');
ylabel('Current (mA)');

% Display the result
disp ('Reference Measured   Error');
disp ('|     mA| |    mA| |    %|');
dispResult = sprintf('%8d %8d %8.3f \n', [I1;  round(I13); err3]);
disp(dispResult)
%
fprintf('MaxErr: %f\n', max(abs(err3)));